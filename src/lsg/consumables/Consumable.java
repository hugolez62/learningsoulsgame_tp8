package lsg.consumables;

import lsg.bags.Collectible;
import lsg.exceptions.ConsumeEmptyException;

public class Consumable implements Collectible {

    private String name;
    private int capacity;
    private String stat;
    private static int WEIGHT = 1;


    public Consumable(String name, int capacity, String stat) {
        this.name = name;
        this.capacity = capacity;
        this.stat = stat;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }

    public int use() throws ConsumeEmptyException {
        if (this.capacity == 0) {
            throw new ConsumeEmptyException();
        }
        int valeurInitiale = this.capacity;
        this.setCapacity(0);
        return valeurInitiale;
    }

    @Override
    public int getWeight() {
        return WEIGHT;
    }

    public String toString() {
        return this.getName() +
                " [" + this.getCapacity() +
                " " + this.getStat() +
                " point(s)]";
    }

    public void printStats() {
        System.out.println(this.toString());
    }
}
