package lsg.consumables;

import lsg.consumables.drinks.Coffee;
import lsg.consumables.drinks.Whisky;
import lsg.consumables.drinks.Wine;
import lsg.consumables.food.Americain;
import lsg.consumables.food.Hamburger;
import lsg.consumables.repair.RepairKit;

import java.util.Iterator;
import java.util.LinkedHashSet;

public class MenuBestOfV4 extends LinkedHashSet<Consumable> {

    public MenuBestOfV4() {
        this.add(new Hamburger());
        this.add(new Wine());
        this.add(new Americain());
        this.add(new Coffee());
        this.add(new Whisky());
        this.add(new RepairKit());
    }

    public String toString() {

        int i = 1;
        Iterator it = super.iterator();

        StringBuilder value = new StringBuilder(this.getClass().getSimpleName() + " :\n");

        while(it.hasNext()) {
            value.append(i).append(" : ").append(it.next()).append("\n");
            i++;
        }

        return value.toString();
    }

    public static void main(String[] args) {

        MenuBestOfV4 menu = new MenuBestOfV4();
        System.out.println(menu.toString());
    }
}