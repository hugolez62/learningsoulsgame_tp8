package lsg;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import lsg.characters.Hero;
import lsg.characters.Zombie;
import lsg.graphics.ImageFactory;
import lsg.graphics.panes.*;
import lsg.graphics.widgets.characters.renderers.HeroRenderer;
import lsg.graphics.widgets.characters.renderers.ZombieRenderer;
import lsg.weapons.Sword;

public class LearningSoulsGameApplication extends Application {

    private Scene scene;
    private AnchorPane root;
    private TitlePane gameTitle;
    private CreationPane creationPane;
    private String heroName;
    private AnimationPane animationPane;
    private Hero hero;
    private HeroRenderer heroRenderer;
    private Zombie zombie;
    private ZombieRenderer zombieRenderer;
    private HUDPane hudPane;

    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Learning Souls Game");
        root = new AnchorPane();
        scene = new Scene(root, 1200, 800);
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        buildUI();
        addListeners();
        primaryStage.show();
        startGame();
    }

    public void createHero() {
        this.hero = new Hero();
        this.hero.setWeapon(new Sword());
        heroRenderer = animationPane.createHeroRenderer();
        this.heroRenderer.goTo(this.animationPane.getPrefWidth()*0.5 - heroRenderer.getFitWidth()*0.65, null);
    }

    private void createMonster(EventHandler<ActionEvent> finishedHandler) {
        this.zombie = new Zombie();
        zombieRenderer = animationPane.createZombieRenderer();
        zombieRenderer.goTo(this.animationPane.getPrefWidth()*0.5 - zombieRenderer.getBoundsInLocal().getWidth() * 0.15, finishedHandler); ;
    }

    private void buildUI() {
        //this.scene.setUserAgentStylesheet(CSSFactory.getStyleSheet("LSG.css"));

        BackgroundImage myBI= new BackgroundImage(
                new Image("lsg/graphics/images/BACKGROUND.png",1200,800,false,true),
                BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);
        this.root.setBackground(new Background(myBI));

        this.gameTitle = new TitlePane(scene, "Learning Souls Game");
        this.creationPane = new CreationPane();
        this.animationPane = new AnimationPane(root);
        this.hudPane = new HUDPane();

        root.getChildren().add(gameTitle);
        root.getChildren().add(creationPane);

        AnchorPane.setLeftAnchor(gameTitle, 0.0);
        AnchorPane.setRightAnchor(gameTitle, 0.0);

        AnchorPane.setLeftAnchor(creationPane, 0.0);
        AnchorPane.setRightAnchor(creationPane, 0.0);
        AnchorPane.setTopAnchor(creationPane, 0.0);
        AnchorPane.setBottomAnchor(creationPane, 0.0);
        creationPane.setAlignment(Pos.CENTER);
        this.creationPane.setStyle("-fx-opacity: 0;");

        AnchorPane.setLeftAnchor(hudPane, 0.0);
        AnchorPane.setRightAnchor(hudPane, 0.0);
        AnchorPane.setTopAnchor(hudPane, 0.0);
        AnchorPane.setBottomAnchor(hudPane, 0.0);
    }

    private void play() {
        root.getChildren().add(this.animationPane);
        root.getChildren().add(this.hudPane);
        createHero();
        createMonster(actionEvent -> {
            this.hudPane.getMessagePane().showMessage("FIGHT !");
            this.hudPane.getMessagePane().translateY( event -> {
                this.hudPane.getMessagePane().fadeOut( event1 -> {
                    root.getChildren().remove(this.hudPane);
                });
            });
        });
    }

    private void addListeners() {
        creationPane.getNameField().setOnAction((event -> {
            heroName = creationPane.getNameField().getText();
            System.out.println("Nom du héro : " + heroName);
            if(heroName != null) {
                root.getChildren().remove(creationPane);
            }
            this.gameTitle.zoomOut(event2 -> {
                System.out.println("ZOOM initial !");
                play();
            });
        }));
    }

    private void startGame() {
        this.gameTitle.zoomIn(event -> {
            this.creationPane.fadeIn(event1 -> {
                ImageFactory.preloadAll((() -> {
                    System.out.println("Pré-chargement des images terminé");
                }));
            }, creationPane);
        });
    }
}
