package lsg;

import lsg.armor.ArmorItem;
import lsg.armor.BlackWitchVeil;
import lsg.armor.DragonSlayerLeggings;
import lsg.armor.RingedKnightArmor;
import lsg.bags.Collectible;
import lsg.bags.MediumBag;
import lsg.bags.SmallBag;
import lsg.buffs.rings.DragonSlayerRing;
import lsg.buffs.rings.Ring;
import lsg.buffs.rings.RingOfDeath;
import lsg.buffs.talismans.NoonGift;
import lsg.characters.Hero;
import lsg.characters.Lycanthrope;
import lsg.characters.Monster;
import lsg.consumables.Consumable;
import lsg.consumables.MenuBestOfV4;
import lsg.consumables.drinks.Coffee;
import lsg.consumables.drinks.Whisky;
import lsg.consumables.food.Hamburger;
import lsg.consumables.repair.RepairKit;
import lsg.exceptions.*;
import lsg.weapons.Claw;
import lsg.weapons.ShotGun;
import lsg.weapons.Sword;
import lsg.weapons.Weapon;

import java.util.Scanner;

public class LearningSoulsGame {

    private Hero hero;
    private Monster monster;
    private Scanner scanner;
    private MenuBestOfV4 menu;
    private static LearningSoulsGame game;
    public static final String BULLET_POINT = "\u2219";

    public static void main(String args[]) {
        game = new LearningSoulsGame();
        game.init();
        game.testExceptions();
        //game.refresh();
    }

    private void refresh() {
        hero.printStats();
        hero.printArmor();
        hero.printRings();
        hero.printConsumable();
        hero.printWeapon();
        hero.printBag();
        monster.printStats();
        monster.printWeapon();
    }

    private void init() {
        hero = new Hero();
        hero.setWeapon(new Sword());
        hero.setConsumable(new RepairKit());
        ArmorItem item1 = new DragonSlayerLeggings();
        Ring ring2 = new DragonSlayerRing();
        Ring ring1 = new RingOfDeath();
        //hero.pickUp(item1);
        //hero.pickUp(ring1);
        //hero.pickUp(ring2);
        //hero.equip(item1, 1);
        //hero.equip(ring1, 1);
        //hero.equip(ring2, 2);
        monster = new Lycanthrope();
        scanner = new Scanner(System.in);
    }

    private void createExhaustedHero() {
        hero = new Hero();
        hero.getHitWith(99);
        hero.setWeapon(new Weapon("Grosse Arme", 0, 0, 1000, 100));
        hero.setStamina(0);
        hero.setConsumable(new Hamburger());
        hero.printStats();
        monster = new Lycanthrope();
        try {
            hero.attack();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void title() {

        String title = "###############################\n";
        title       += "#   THE LEARNING SOULS GAME   #\n";
        title       += "###############################\n";

        System.out.println(title);
    }

    private void testExceptions() {
        //hero.setWeapon(null);
        //hero.setStamina(0);
        hero.setBag(null);
        hero.printStats();
        hero.printArmor();
        hero.printWeapon();
        hero.printRings();
        hero.printConsumable();
        hero.printBag();
        game.fight1v1();
    }

    private void fight1v1() {
        int heroAttack;
        String heroWeapon;
        int monsterAttack;
        int action = 0;
        String vainqueur;
        int tourDeJeu = 0;

        title();

        while (hero.isAlive() && monster.isAlive() ) {

            if (tourDeJeu % 2 == 0) {
                do {
                    System.out.print("Hero's action for next move : (1) attack | (2) consume > ");

                    action = scanner.nextInt();

                    if(action == 1) {
                        System.out.println();
                        try {
                            heroAttack = hero.attack();
                        } catch (WeaponNullException e) {
                            heroAttack = 0;
                            System.out.println("WARNING : no weapon has been equiped !!!");
                        } catch (WeaponBrokenException e) {
                            heroAttack = 0;
                            System.out.println(hero.getWeapon().getName() + " is broken !!!");
                        } catch (StaminaEmptyException e ) {
                            heroAttack = 0;
                            System.out.println("ACTION HAS NO EFFECT : no more stamina !!!");
                        }

                        System.out.println(hero.getName() + " attack " + monster.getName() + " with " + hero.getWeapon() + " (ATTACK: " + heroAttack + " | DMG: " + monster.getHitWith(heroAttack) + ")");
                        System.out.println();
                    }

                    if(action == 2) {
                        System.out.println();
                        try {
                            hero.consume();
                        } catch (ConsumeNullException e) {
                            System.out.println("IMPOSSIBLE ACTION : no consumable has been equiped !");
                        } catch (ConsumeEmptyException e) {
                            System.out.println("ACTION HAS NOT EFFECT : " + hero.getConsumable().getName() + " is empty !");
                        } catch (ConsumeRepairNullWeaponException e) {
                            System.out.println("IMPOSSIBLE ACTION : no weapon has been equiped !");
                        }
                    }
                } while(action != 1 && action != 2);
            }

            if (tourDeJeu % 2 != 0) {
                try {
                    monsterAttack = monster.attack();
                } catch (WeaponNullException e) {
                    monsterAttack = 0;
                    System.out.println("WARNING : no weapon has been equiped !!!");
                } catch (WeaponBrokenException e) {
                    monsterAttack = 0;
                    System.out.println(hero.getWeapon().getName() + " is broken");
                } catch (StaminaEmptyException e ) {
                    monsterAttack = 0;
                    System.out.println("ACTION HAS NO EFFECT : no more stamina !!!");
                }
                System.out.println(monster.getName() + " attack " + hero.getName() + " with " + monster.getWeapon().getName() + " (ATTACK: " + monsterAttack + " | DMG: " + hero.getHitWith(monsterAttack)+")");
                System.out.println();
            }
            tourDeJeu++;

            refresh();
            System.out.println();
        }

        vainqueur = hero.isAlive()? hero.getName() : monster.getName();
        System.out.println("--- "+vainqueur+" WINS !!! ---");

    }

}
