package lsg.bags;

import lsg.armor.ArmorItem;
import lsg.armor.BlackWitchVeil;
import lsg.armor.DragonSlayerLeggings;
import lsg.consumables.Consumable;
import lsg.consumables.food.Hamburger;
import lsg.weapons.Sword;
import lsg.weapons.Weapon;

public class SmallBag extends Bag {

    public SmallBag() {
        super(10);
    }

    public static void main(String[] args) {
        SmallBag bag = new SmallBag();
        ArmorItem item1 = new BlackWitchVeil();
        ArmorItem item2 = new DragonSlayerLeggings();
        Weapon item3 = new Sword();
        Consumable item4 = new Hamburger();

        System.out.println(bag.toString());

        bag.pop(item2);

        System.out.println();

        System.out.println(bag.toString());

    }
}
