package lsg.graphics.panes;

import javafx.geometry.Pos;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;

public class HUDPane extends BorderPane {

    private MessagePane messagePane;

    public HUDPane() {
        buildCenter();
    }

    public MessagePane getMessagePane() {
        return this.messagePane;
    }

    public void buildCenter() {
        messagePane = new MessagePane();
        AnchorPane.setLeftAnchor(messagePane, 0.0);
        AnchorPane.setRightAnchor(messagePane, 0.0);
        AnchorPane.setTopAnchor(messagePane, 0.0);
        AnchorPane.setBottomAnchor(messagePane, 0.0);
        this.getChildren().add(messagePane);
    }
}
