package lsg.graphics.panes;

import javafx.animation.FadeTransition;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.layout.VBox;
import javafx.util.Duration;
import lsg.graphics.widgets.texts.GameLabel;

public class MessagePane extends VBox {

    private GameLabel gameLabel;

    public void showMessage(String msg) {
        this.gameLabel = new GameLabel(msg);
        this.getChildren().addAll(gameLabel);
        gameLabel.setMinWidth(1200);
        gameLabel.setTranslateY(350);
        gameLabel.setAlignment(Pos.CENTER);
    }

    public void translateY(EventHandler<ActionEvent> finishedHandler) {

        TranslateTransition tt = new TranslateTransition(Duration.millis(2000));
        tt.setToY(150);
        tt.setNode(gameLabel);
        tt.setCycleCount(1);
        tt.setOnFinished(finishedHandler);
        tt.play();
    }

    public void fadeOut(EventHandler<ActionEvent> finishedHandler) {

        FadeTransition ft = new FadeTransition(Duration.millis(3000));
        ft.setFromValue(1.0);
        ft.setToValue(0.0);
        ft.setNode(gameLabel);
        ft.play();
    }
}