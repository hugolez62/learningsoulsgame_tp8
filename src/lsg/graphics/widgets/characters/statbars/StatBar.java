package lsg.graphics.widgets.characters.statbars;

import javafx.scene.layout.BorderPane;

public class StatBar extends BorderPane {

    public StatBar () {
        this.setMinSize(350, 100);
        this.setStyle("-fx-border-color: red");
    }
}
