package lsg.helpers;

import java.util.Random;

public class Dice {

    public static void main(String[] args) {

        int nombre = 0;
        int min = 24;
        int max = 25;

        Dice des = new Dice(50);

        for(int i = 0; i<501;i++) {
            nombre = des.roll();
            System.out.print(nombre + " ");

            if (nombre <= min) {
                min = nombre;
            } else if(nombre >= max){
                max = nombre;
            }
        }

        System.out.println(min+ " - " +max);
    }

    private int faces;
    private Random random;

    public int getFaces() {
        return faces;
    }

    public void setFaces(int faces) {
        this.faces = faces;
    }

    public Random getRandom() {
        return random;
    }

    public void setRandom(Random random) {
        this.random = random;
    }

    public Dice(int faces) {
        this.faces = faces;
        this.random = new Random(5342);
    }

    public int roll() {
        return (this.random.nextInt(this.faces-1));
    }
}


