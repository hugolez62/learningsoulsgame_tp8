package lsg.characters;

import lsg.armor.ArmorItem;
import lsg.armor.BlackWitchVeil;
import lsg.armor.DragonSlayerLeggings;
import lsg.armor.RingedKnightArmor;
import lsg.bags.Collectible;
import lsg.buffs.rings.Ring;
import lsg.buffs.rings.RingOfDeath;
import lsg.exceptions.NoBagException;
import lsg.helpers.Dice;

public class Hero extends Character {


    private static final String DEFAULTNAME = "Gregooninator";
    private static int DEFAULT_LIFE = 100;
    private static int DEFAULT_STAMINA = 50;
    private static int MAX_ARMOR_PIECES = 3;
    private static int MAX_RINGS = 2;
    private ArmorItem[] armor;

    private Ring rings[];

    public Hero() {
        this.setName(DEFAULTNAME);
        this.setLife(DEFAULT_LIFE);
        this.setMaxLife(DEFAULT_LIFE);
        this.setStamina(DEFAULT_STAMINA);
        this.setMaxStamina(DEFAULT_STAMINA);
        this.setDice(new Dice(101));
        this.armor = new ArmorItem[MAX_ARMOR_PIECES];
        this.rings = new Ring[MAX_RINGS];
    }

    public Hero(String name) {
        this.setName(name);
        this.setLife(DEFAULT_LIFE);
        this.setMaxLife(DEFAULT_LIFE);
        this.setStamina(DEFAULT_STAMINA);
        this.setMaxStamina(DEFAULT_STAMINA);
        this.setDice(new Dice(101));
        this.armor = new ArmorItem[MAX_ARMOR_PIECES];
        this.rings = new Ring[MAX_RINGS];
    }

    public static void main(String[] args) {
        Hero hero = new Hero();
        ArmorItem item1 = new DragonSlayerLeggings();
        ArmorItem item2 = new BlackWitchVeil();
        Ring item3 = new RingOfDeath();
    }

    public void setArmorItem(ArmorItem item, int nSlot) {

        if (nSlot > 0 && nSlot <= MAX_ARMOR_PIECES) {
            int slot = nSlot - 1;
            armor[slot] = item;
        }
    }

    public Ring[] getRings() {
        return rings;
    }

    public void setRing(Ring item, int nSlot) {
        if (nSlot > 0 && nSlot <= MAX_RINGS) {
            int slot = nSlot - 1;
            rings[slot] = item;
            item.setHero(this);
        }
    }

    public float getTotalArmor() {
        return ((armor[0] == null ? 0 : armor[0].getArmorValue()) +
                (armor[1] == null ? 0 : armor[1].getArmorValue()) +
                (armor[2] == null ? 0 : armor[2].getArmorValue()));
    }

    private float getTotalBuff() {
        return ((rings[0] == null ? 0 : rings[0].computeBuffValue()) +
                (rings[1] == null ? 0 : rings[1].computeBuffValue()));
    }

    public String armorToString() {

        String value = String.format("%-7s ", "ARMOR");

        value += String.format("%-34s", this.armor[0] == null ? "1:empty" : "1:" + this.armor[0].getName() + "(" + this.armor[0].getArmorValue() + ")");
        value += String.format("%-34s", this.armor[1] == null ? "2:empty" : "2:" + this.armor[1].getName() + "(" + this.armor[1].getArmorValue() + ")");
        value += String.format("%-32s", this.armor[2] == null ? "3:empty" : "3:" + this.armor[2].getName() + "(" + this.armor[2].getArmorValue() + ")");
        value += "TOTAL:" + this.getTotalArmor();
        return value;
    }

    public String ringsToString() {

        String value = String.format("%-7s ", "RINGS");

        value += String.format("%-34s", this.rings[0] == null ? "1:empty" : "1:" + this.rings[0].getName() + "(" + this.rings[0].computeBuffValue() + ")");
        value += String.format("%-34s", this.rings[1] == null ? "2:empty" : "2:" + this.rings[1].getName() + "(" + this.rings[1].computeBuffValue() + ")");
        value += "TOTAL:" + this.getTotalBuff();
        return value;
    }

    public void printArmor() {
        System.out.println(this.armorToString());
    }

    public void printRings() {
        System.out.println(this.ringsToString());
    }

    protected float computeProtection() {

        return getTotalArmor();
    }

    protected float computeBuff() {

        return getTotalBuff();
    }

    public ArmorItem[] getArmorItems() {

        int compteur = 0;
        int compteurBis = 0;
        ArmorItem[] armorReturn;

        for (int i = 0; i < armor.length; i++) {
            if (armor[i] != null) compteur++;
        }

        armorReturn = new ArmorItem[compteur];

        for (int i = 0; i < armor.length; i++) {
            if (armor[i] != null) {
                armorReturn[compteurBis] = armor[i];
                compteurBis++;
            }

        }

        return armorReturn;
    }

    public void equip(ArmorItem item, int slot) throws NoBagException {
        if (this.getBag().contains(item) && this.getBag().contains(item)) {
            this.setArmorItem(item, slot);
            pullOut(item);
            System.out.println(" and equips it !");
        }


    }

    public void equip(Ring ring, int slot) throws NoBagException {
        if (this.getBag().contains(ring) && this.getBag().contains(ring)) {
            this.setRing(ring, slot);
            pullOut(ring);
            System.out.println(" and equips it !");
        }
    }
}
