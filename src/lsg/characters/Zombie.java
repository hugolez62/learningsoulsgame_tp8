package lsg.characters;

import lsg.weapons.Claw;
import lsg.weapons.Sword;
import lsg.weapons.Weapon;

public class Zombie extends Monster {

    private static final String DEFAULTNAME = "Zombie";

    public Zombie() {
        this.setName(DEFAULTNAME);
        this.setLife(10);
        this.setStamina(10);
        this.setWeapon(new Weapon("Zombie's hands", 5, 20, 1, 1000));
        this.setSkinThickness(10);
    }
}

