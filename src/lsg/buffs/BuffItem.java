package lsg.buffs;

import lsg.bags.Collectible;

import java.util.Locale;


public abstract class BuffItem {
	
	private String name ;
	private static String BUFF_STAT_STRING= "BUFF:";


	public BuffItem(String name) {
		this.name = name ;
	}
	
	public abstract float computeBuffValue() ;
	
	public String getName() {
		return name;
	}
	
	@Override
	public String toString() {
		return String.format(Locale.US, "[%s, %.2f]", getName(), computeBuffValue()) ;
	}



    public static int getWEIGHT() {
		return 1;
    }
}
